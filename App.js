const express = require('express');
const mongoose = require('mongoose');
const session = require('express-session');
const passport = require('passport');
const authRoutes = require('./routes/auth');
const reviewRoutes = require('./routes/review');
const flash = require('connect-flash');
const Review = require('./models/Review');
const router = express.Router();
const methodOverride = require('method-override');

const app = express();
const port = 3000;


require('./config/database');
require('./config/passport')(passport);

app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));
app.use(methodOverride('_method'));

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}));

app.use(flash());

app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'ejs'); 

app.use('/', authRoutes);
app.use('/', reviewRoutes);
app.use(router);

router.get('/', async (req, res) => {
try {
    const reviews = await Review.find(); 
    res.render('index', { reviews, isAuthenticated: req.isAuthenticated() });
} catch (error) {
    console.error(error);
    res.status(500).send('Chyba při načítání recenzí');
}
});

app.listen(port, () => {
  console.log("\x1b[36m" + `Server běží na portu ${port}: http://localhost:${port}` + "\x1b[0m");
});
