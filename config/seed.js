const User = require('../models/User'); 
const Review = require('../models/Review');
const bcrypt = require('bcrypt')

async function hashPassword(password){
  return await bcrypt.hash(password, 10)
}

const seedUsers = [
  { username: 'admin', email: 'admin@seznam.com', password: "Admin123." },
  { username: 'test', email: 'test@gmail.com', password: "Tester123." },
  { username: 'user1', email: 'user1@example.com', password: "Password123." },
  { username: 'user2', email: 'user2@example.com', password: "Password123." }
];

const seedReviews = [
  { website: 'Google', url: 'www.google.com', rating: 5, review: "velmi dobrý vyhledávač, mohu jedině doporučit", author: "admin" },
  { website: 'Seznam', url: 'www.seznam.cz', rating: 3, review: "Dobrá stránka, používám každý den.", author: "test" },
  { website: 'Amazon', url: 'www.amazon.com', rating: 1, review: "Naprd, balík mi hodí před dveře a rozbije se mi zboží.", author: "user1" },
  { website: 'test', url: 'www.test.com', rating: 2, review: "test recenze 123456", author: "test" },
];

async function hashPassword(password) {
  return bcrypt.hash(password, 10);
}

async function seedDB() {
  try {
    await User.deleteMany(); 
    for (let user of seedUsers) {
      const hashedPassword = await hashPassword(user.password);
      user.password = hashedPassword;
    }
    await User.insertMany(seedUsers);

    await Review.deleteMany(); 
    await Review.insertMany(seedReviews);

    console.log("\x1b[32m" + 'Data úspěšně naseedovány' + "\x1b[0m");
  } catch (err) {
    console.error("\x1b[31m" + 'Error seeding data:', err);
  }
}

module.exports = seedDB;