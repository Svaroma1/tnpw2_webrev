const mongoose = require('mongoose');
const seedDB = require('./seed.js');

const dbURI = 'mongodb://localhost:27017/webrev';

mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("\x1b[32m" + 'MongoDB připojen' + "\x1b[0m");
    seedDB()
  })
  .catch(err => console.log(err));
