const express = require('express');
const bcrypt = require('bcryptjs'); 
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');

const router = express.Router();
const User = require('../models/User');

// Middleware pro express-session
router.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

// Middleware pro connect-flash
router.use(flash());

// Globální proměnné pro vyskakovací zprávy
router.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  next();
});

// GET pro zobrazení registračního okna
router.get('/register', (req, res) => {
  res.render('register');
});

// POST pro založení uživatele
router.post('/register', async (req, res) => {
  const { username, email, password } = req.body;
  try {
    // Check if user or email already exists
    let user = await User.findOne({ $or: [{ username }, { email }] });
    if (user) {
      req.flash('error_msg', 'Uživatel nebo email již existuje.');
      return res.redirect('/register');
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    user = new User({ username, email, password: hashedPassword });
    await user.save();

    req.flash('success_msg', 'Jste registrováni a můžete se přihlásit.');
    res.redirect('/login');
  } catch (err) {
    console.error(err);
    req.flash('error_msg', 'Nastala chyba při registraci.');
    res.redirect('/register');
  }
});


// GET pro zobrazení přihlašovacího formuláře
router.get('/login', (req, res) => {
  res.render('login');
});

// POST pro přihlášení uživatele
router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
  })(req, res, next);
});

// GET pro odhlášení
router.get('/logout', (req, res) => {
  req.logout(function(err) {
    if (err) { return next(err); }
    res.redirect('/');
  });
});

module.exports = router;
