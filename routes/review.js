const express = require('express');
const router = express.Router();
const Review = require('../models/Review'); 

// Middleware pro kontrolu, jestli je uživatel přihlášený
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

// GET pro zobrazení formuláře na tvorbu nové recenze
router.get('/reviews/new', (req, res) => {
    res.render("new", { isAuthenticated: req.isAuthenticated() })
  });

// GET pro Získání všech recenzí
router.get('/reviews', async (req, res) => {
    try {
        const reviews = await Review.find();
        res.json(reviews);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// GET pro získání jedné recenze podle ID
router.get('/reviews/:id', getReview, (req, res) => {
    res.render("show", { review: res.review, isAuthenticated: req.isAuthenticated() });
});

// GET pro získání všech recenzí přihlášeného uživatele
router.get('/myreviews', ensureAuthenticated, async (req, res) => {
    try {
        const userReviews = await Review.find({ author: req.user.username });

        res.render('showall', { reviews: userReviews, isAuthenticated: req.isAuthenticated() });
    } catch (error) {
        console.error(error);
        res.status(500).send('Error retrieving user reviews');
    }
});

// POST pro vytvoření nové recenze
router.post('/reviews', ensureAuthenticated, async (req, res) => {
    const review = new Review({
        website: req.body.website,
        url: req.body.url,
        rating: req.body.rating,
        review: req.body.review,
        author: req.user.username
    });

    try {
        const newReview = await review.save();
        res.redirect("/");
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// GET pro otevření formuláře pro editování recenze
router.get('/edit-review/:id', getReview, (req, res) => {
    res.render("edit", { review: res.review, isAuthenticated: req.isAuthenticated() });
});

// PATCH pro úpravu recenze
router.patch('/edit-review/:id', getReview, async (req, res) => {
    if (req.body.website != null) {
        res.review.website = req.body.website;
    }
    if (req.body.url != null) {
        res.review.url = req.body.url;
    }
    if (req.body.rating != null) {
        res.review.rating = req.body.rating;
    }
    if (req.body.review != null) {
        res.review.review = req.body.review;
    }
    if (req.body.author != null) {
        res.review.author = req.body.author;
    }
    try {
        const updatedReview = await res.review.save();
        res.redirect("/myreviews")
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// DELETE pro smazání recenze podle id
router.delete('/reviews/:id', getReview, async (req, res) => {
    try {
        if (req.user.username !== res.review.author) {
            return res.status(403).json({ message: "You can only delete your own reviews." });
        }
        await res.review.deleteOne();
        res.redirect("/myreviews");
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Middleware pro získání recenze podle ID
async function getReview(req, res, next) {
    let review;
    try {
        review = await Review.findById(req.params.id);
        if (review == null) {
            return res.status(404).json({ message: 'Cannot find review' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }

    res.review = review;
    next();
}

// GET pro vyhledávání v recenzích
router.get('/search', async (req, res) => {
    let searchQuery = req.query.query;
    let reviews;
    try {
        if (searchQuery) {
            reviews = await Review.find({ $text: { $search: searchQuery } });
        } else {
            reviews = await Review.find({});
        }
        res.render('search', { reviews: reviews, searchQuery: searchQuery || '', isAuthenticated: req.isAuthenticated() });
    } catch (err) {
        console.error(err);
        res.status(500).send('Nastala chyba při vyhledávání.');
    }
});

module.exports = router;
